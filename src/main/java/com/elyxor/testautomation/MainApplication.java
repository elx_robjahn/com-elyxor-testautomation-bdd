package com.elyxor.testautomation;

import com.elyxor.testautomation.configuration.ConfigurationSource;
import com.elyxor.testautomation.configuration.PropertiesConfigurationSource;
import com.elyxor.testautomation.testexecutionengine.TestExecutionEngine;
import com.elyxor.testautomation.testexecutionengine.testng.TestNgExecutionEngine;
import com.elyxor.testautomation.testmanagementservice.TestManagementService;
import com.elyxor.testautomation.testmanagementservice.TestManagementServiceRegistry;
import com.elyxor.testautomation.testmanagementservice.testrail.TestRailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.TestNG;
import org.testng.collections.Lists;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainApplication {

    private static File BUILD_DIR = new File("build");
    private static String PACKAGE_NAME = "com.elyxor.testautomation.tests";
    private static Logger logger = LoggerFactory.getLogger(MainApplication.class);

    public static void main(String[] args){
        TestNG testng = new TestNG();

        //logger.debug("TR prop: {}", System.getProperty("testRunId"));

        //by passing in testRailId - assume we're retrieving list of test cases from TestRail
        //if not, default to defining test cases manually
        String testRailRunId ="";

        Map<String, String> testSpec = null;
        TestExecutionEngine executionEngine = new TestNgExecutionEngine(BUILD_DIR);
        executionEngine.setTestPackage(PACKAGE_NAME);

        if(System.getProperty("testRunId") !=null){
            testRailRunId = System.getProperty("testRunId");
            logger.debug("TestRun ID provided: {}", testRailRunId);
            try{
                ConfigurationSource testRailProps = new PropertiesConfigurationSource("classpath*:testrailconfig.properties");
                TestManagementServiceRegistry registry = TestManagementServiceRegistry.getInstance();
                TestManagementService testRailService = registry.registerService(new TestRailService(testRailProps));
                testSpec = testRailService.getTestSpec(testRailRunId);
            }catch (Exception e){
                logger.error("Error configuring TestRail", e);
            }
        }else{
            testSpec = new HashMap<>();
            testSpec.put("TestGetBookByISBN", "1");
        }
        executionEngine.generateTestExecutionSpec(testSpec);

        testng.setUseDefaultListeners(false);

        List<String> suites = Lists.newArrayList();
        suites.add("build/generated/testng.xml");
        testng.setTestSuites(suites);
        testng.run();

    }
}
