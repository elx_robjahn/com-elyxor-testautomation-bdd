package com.elyxor.testautomation.tests;

import cucumber.api.CucumberOptions;

@CucumberOptions(
		features = "src/test/features/GetBookByISBN.feature")
public class TestGetBookByISBN extends BaseTest {
}
