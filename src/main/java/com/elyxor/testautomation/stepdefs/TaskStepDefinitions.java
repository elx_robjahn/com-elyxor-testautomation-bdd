package com.elyxor.testautomation.stepdefs;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;

public class TaskStepDefinitions {

	private Response response;
	private ValidatableResponse json;
	private RequestSpecification request;

	private String BASEURI = "http://localhost:8080/";
	private String ENDPOINT = "http://localhost:8080/";

	@Given("tasks exists")
	public void tasks_exist(){
		ENDPOINT += "gettask";
		request = given();
	}

	@Given("a task exists with a task name of (.*)")
	public void a_task_exists_with_a_task_name_of(String task_name){
		ENDPOINT += "gettask/" + task_name;
		request = given();
	}

	@When("a user retrieves web service response")
	public void a_user_retrieves_web_service_response(){
		response = request.when().baseUri(BASEURI).get(ENDPOINT);
		//System.out.println("response: " + response.prettyPrint());
	}

	@Then("response status code is (\\d+)")
	public void response_status_code(int statusCode){
		json = response.then().statusCode(statusCode);
	}

	@And("response fields match the following$")
	public void response_equals(Map<String,String> responseFields){
		for (Map.Entry<String, String> field : responseFields.entrySet()) {
			if(StringUtils.isNumeric(field.getValue())){
				json.body(field.getKey(), equalTo(Integer.parseInt(field.getValue())));
			}
			else{
				json.body(field.getKey(), equalTo(field.getValue()));
			}
		}
	}

	@And("response fields include the following in any order")
	public void response_contains_in_any_order(Map<String,String> responseFields){
		for (Map.Entry<String, String> field : responseFields.entrySet()) {
			if(StringUtils.isNumeric(field.getValue())){
				json.body(field.getKey(), containsInAnyOrder(Integer.parseInt(field.getValue())));
			}
			else{
				json.body(field.getKey(), containsInAnyOrder(field.getValue()));
			}
		}
	}
}


