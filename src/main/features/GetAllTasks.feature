Feature: Get all tasks
  Scenario: User calls get all tasks web service
	Given tasks exists
	When a user retrieves web service response
	Then response status code is 200
	And response fields match the following
		  | tasks.t21 | Task 21 |
		  | tasks.t22 | Task 22 |
